# Animate Widgets

Animate Widgets is a WordPress plugin that adds additional widget fields into existing widget forms and uses [Animate On Scroll](https://michalsnik.github.io/aos/) to render animation to widgets.

## Installation
1. Download and extract the [zip](https://bitbucket.org/recommendwp/animate-widgets/get/3c9e5eb0ba0e.zip) file into your plugins directory.
2. Activate the plugin.
3. Navigate to your `Appearance > Widgets` and enjoy.

## Credits

* [Animate on Scroll](https://michalsnik.github.io/aos/)
* [WordPress Stackexchange Question](https://wordpress.stackexchange.com/questions/134539/how-to-add-custom-fields-to-settings-in-widget-options-for-all-registered-widget)
* [So Page Builder Animate](https://wordpress.org/plugins/so-page-builder-animate/)

## TODO

* ~~Add support for widgets added through SiteOrigin Panels~~
* Additional widget fields for Animate on Scroll settings
* ~~Add css styling on the fields~~
* WordPress repository release